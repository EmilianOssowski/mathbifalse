import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegulaFalseComponent } from './regula-false.component';

describe('RegulaFalseComponent', () => {
  let component: RegulaFalseComponent;
  let fixture: ComponentFixture<RegulaFalseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegulaFalseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegulaFalseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
