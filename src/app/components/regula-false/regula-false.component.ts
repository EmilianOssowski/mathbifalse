import {Component, OnInit} from '@angular/core';
import {RegulaFalsiService} from '../../services/regula-falsi.service';
import * as Chart from 'node_modules/chart.js';

@Component({
  selector: 'app-regula-false',
  templateUrl: './regula-false.component.html',
  styleUrls: ['./regula-false.component.css']
})
export class RegulaFalseComponent implements OnInit {

  data: any;
  // options: any;
  xAxis = [-7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7];
  finalResult;
  finalValue;
  steps;
  actualStep;
  actualRangeStart;
  actualRangeEnd;
  functionPolynomial;
  public graph;
  constructor(private p: RegulaFalsiService) {
  }

  ngOnInit() {
    this.initializeXAxis();
    this.graph = {
      data: [
        { x: this.xAxis, y: [], type: 'linear', mode: 'logarithmic', marker: {color: 'red'} }
      ],
    };

    this.p.polynomial.subscribe((res) => {
        const results: Array<number> = new Array<number>();
        this.xAxis.forEach((x) => {
          results.push(this.p.calculateY(x));
        });
        // console.log(results);
        this.functionPolynomial = results;
        this.actualRangeEnd = this.p.getRangeEnd();
        this.actualRangeStart = this.p.getRangeStart();
        this.appendNewData();
      }
    );
  }
  initializeXAxis() {
    this.xAxis = [-5];
    let i = 1;
    while (i !== 100) {
      this.xAxis.push(this.xAxis[i - 1] + 0.1);
      i = i + 1;
    }
    // console.log(this.xAxis);
  }
  findRange() {
    const range = [];
    this.xAxis.forEach( x => {
      if (this.p.getRangeStart() !== x || this.p.getRangeEnd() !== x) {
        range.push(null);
      } else {
        if (this.p.getRangeStart() === x) {
          range.push(0);
        } else {
          range.push(0);
        }
      }
    });
    return range;
  }
  appendNewData() {
    this.graph = {
      data: [
        { x: this.xAxis, y: this.functionPolynomial, type: 'linear', mode: 'logarithmic', marker: {color: 'red'}, name: 'Wielomian' },
        { x: this.xAxis, y: this.findRange(), type: 'linear', mode: 'logarithmic', marker: {color: 'green'} , name: 'Zakres'},
      ]};
  }
  nextStep() {
    this.actualStep = this.p.getIt();
    const res = this.p.getNextIteration();
    this.steps = this.p.request.getValue().length;
    this.graph = {
      data: [
        { x: this.xAxis, y: this.functionPolynomial, type: 'linear', mode: 'logarithmic', marker: {color: 'red'}, name: 'Wielomian' },
        { x: [res.lines.startX, res.lines.endX], y: [res.lines.startY, res.lines.endY], type: 'linear', mode: 'logarithmic',
          marker: {color: 'gray'} , name: 'Prosta'},
        { x: [res.polynomial.key], y: [res.polynomial.value], type: 'linear', mode: 'logarithmic', marker: {color: 'green'} , name: 'Wynik'},
      ]};
    this.finalResult = res.polynomial.key;
    this.finalValue = res.polynomial.value;
  }

  previousStep() {
    this.actualStep = this.p.getIt();
    const res = this.p.getPreviousIteration();
    this.steps = this.p.request.getValue().length;
    if (res !== 0) {
      this.graph = {
        data: [
          {
            x: this.xAxis,
            y: this.functionPolynomial,
            type: 'linear',
            mode: 'logarithmic',
            marker: {color: 'red'},
            name: 'Wielomian'
          },
          {
            x: [res.lines.startX, res.lines.endX],
            y: [res.lines.startY, res.lines.endY],
            type: 'linear',
            mode: 'logarithmic',
            marker: {color: 'grey'},
            name: 'Prosta'
          },
          {
            x: [res.polynomial.key], y: [res.polynomial.value],
            type: 'linear',
            mode: 'logarithmic',
            marker: {color: 'green'},
            name: 'Wynik'
          },
        ]
      };
      this.finalResult = res.polynomial.key;
      this.finalValue = res.polynomial.value;
    } else {
      this.appendNewData();
    }
  }
  calculateLinearFunction(a, b, args: Array<any> = this.xAxis) {
    const values = [];
    args.forEach((x) => {
      values.push(x * a + b);
    });
    return values;
  }
}
