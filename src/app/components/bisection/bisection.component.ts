import {Component, OnInit} from '@angular/core';
import {BisectionService} from '../../services/bisection.service';

@Component({
  selector: 'app-bisection',
  templateUrl: './bisection.component.html',
  styleUrls: ['./bisection.component.css']
})
export class BisectionComponent implements OnInit {

  data: any;
  options;
  xAxis;
  finalResult;
  finalValue;
  steps;
  actualStep;
  graph;
  functionPolynomial;
  actualRangeStart;
  actualRangeEnd;

  constructor(private p: BisectionService) {
  }

  ngOnInit() {
    this.initializeXAxis();
    this.graph = {
      data: [
        {x: this.xAxis, y: [], type: 'linear', mode: 'logarithmic', marker: {color: 'red'}}
      ],
    };
    this.p.polynomial.subscribe((res: Array<any>) => {
        const results: Array<number> = new Array<number>();
        this.xAxis.forEach((x) => {
          if (res[0] === 'sin') {
            console.log('liczenie sinusa');
            results.push(this.p.calculateSin(x));
          } else {
            results.push(this.p.calculateY(x));
          }
        });
        // console.log(results);
        this.functionPolynomial = results;
        console.log(this.p.getRangeStart() + ':' + this.p.getRangeEnd())
        this.actualRangeStart = this.p.getRangeStart();
        this.actualRangeEnd  = this.p.getRangeEnd();
        this.appendNewData();
      }
    );
    this.p.rangeStart.subscribe(res => {
      this.actualRangeStart = res;
    });
    this.p.rangeEnd.subscribe(res => {
      this.actualRangeEnd = res;
    });
  }

  initializeXAxis() {
    this.xAxis = [-5];
    let i = 1;
    while (i !== 100) {
      this.xAxis.push(this.xAxis[i - 1] + 0.1);
      i = i + 1;
    }
    // console.log(this.xAxis);
  }

  appendNewData() {
    console.log(this.actualRangeStart + ':' + this.actualRangeEnd);
    this.graph = {
      data: [
        {
          x: this.xAxis,
          y: this.functionPolynomial,
          type: 'linear',
          mode: 'logarithmic',
          marker: {color: 'red'},
          name: 'Wielomian'
        },
      ],
      layout: {
        shapes: [
          {
            fillcolor: 'rgba(63, 81, 181, 0.2)',
            line: {width: 0},
            type: 'rect',
            x0: this.actualRangeStart,
            x1: this.actualRangeEnd,
            xref: 'x',
            y0: 100,
            y1: -100,
            yref: 'paper'
          }]
      }
    };
  }

  nextStep() {
    this.getNextRange();
    this.actualStep = this.p.getIt();
    const step = this.p.getNextIteration();
    this.steps = this.p.request.getValue().length;
    this.graph = {
      data: [
        {
          x: this.xAxis,
          y: this.functionPolynomial,
          type: 'linear',
          mode: 'logarithmic',
          marker: {color: 'red'},
          name: 'Wielomian'
        },
        {
          x: [step.key],
          y: [step.value],
          type: 'linear',
          mode: 'logarithmic',
          marker: {color: 'green'},
          name: 'Wynik'
        },
      ],
      layout: {
        shapes: [
          {
            fillcolor: 'rgba(63, 81, 181, 0.2)',
            line: {width: 0},
            type: 'rect',
            x0: this.actualRangeStart,
            x1: this.actualRangeEnd,
            xref: 'x',
            y0: 100,
            y1: -100,
            yref: 'paper'
          }]
      }
    };
    this.finalResult = step.key;
    this.finalValue = step.value;
  }
  getNextRange() {
    const res: any = this.p.getActualRange();
    const next: any = this.p.getNextKey();
    console.log('Zakres: ' + this.actualRangeStart + ':' + this.actualRangeEnd);

    console.log(res);
    if (((Math.abs(this.actualRangeStart) + Math.abs(this.actualRangeEnd)) / 2) <= next.key) {
      this.actualRangeStart = res.key;
    } else {
      this.actualRangeEnd = res.key;
    }
    console.log('Nowy zakres: ' + this.actualRangeStart + ':' + this.actualRangeEnd);
  }
  previousStep() {
    this.actualStep = this.p.getIt();
    this.steps = this.p.request.getValue().length;
    this.getPreviousRange();
    const step = this.p.getPreviousIteration();
    this.graph = {
      data: [
        {
          x: this.xAxis,
          y: this.functionPolynomial,
          type: 'linear',
          mode: 'logarithmic',
          marker: {color: 'red'},
          name: 'Wielomian'
        },
        {
          x: [step.key],
          y: [step.value],
          type: 'linear',
          mode: 'logarithmic',
          marker: {color: 'green'},
          name: 'Wynik'
        },
      ],
      layout: {
        shapes: [
          {
            fillcolor: 'rgba(63, 81, 181, 0.2)',
            line: {width: 0},
            type: 'rect',
            x0: this.actualRangeStart,
            x1: this.actualRangeEnd,
            xref: 'x',
            y0: 100,
            y1: -100,
            yref: 'paper'
          }]
      }};
    this.finalResult = step.key;
    this.finalValue = step.value;
  }
  getPreviousRange() {
    const res: any = this.p.getActualRange();
    const previous: any = this.p.getPreviousKey();
    console.log('Zakres: ' + this.actualRangeStart + ':' + this.actualRangeEnd);

    console.log(res);
    if (((Math.abs(this.actualRangeStart) + Math.abs(this.actualRangeEnd)) / 2) > previous.key) {
      this.actualRangeStart = res.key;
    } else {
      this.actualRangeEnd = res.key;
    }
    console.log('Nowy zakres: ' + this.actualRangeStart + ':' + this.actualRangeEnd);
  }

  findRange() {
    const range = [];
    this.xAxis.forEach(x => {
      if (this.p.getRangeStart() !== x || this.p.getRangeEnd() !== x) {
        range.push(null);
      } else {
        if (this.p.getRangeStart() === x) {
          range.push(0);
        } else {
          range.push(0);
        }
      }
    });
    return range;
  }

}
