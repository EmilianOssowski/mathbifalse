import {Component, OnInit} from '@angular/core';
import {SelectItem} from 'primeng/api';
import {HttpService} from '../../services/http.service';
import {map} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BisectionService} from "../../services/bisection.service";
import {RegulaFalsiService} from "../../services/regula-falsi.service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  types: SelectItem[] = [
    {label: 'Argumenty ', value: false},
    {label: 'Wartości', value: true},
  ];
  selectedType = this.types[0].value;
  rangeStart = 1;
  rangeEnd = 2;
  accuracy = 0.00001;
  polynomial: Array<any> = [];
  polynomialString: string;
  //
  request;
  accuracyRegex = /[0][.]0*[1]/;

  constructor(private http: HttpService, private p: BisectionService, private r: RegulaFalsiService) {
  }

  ngOnInit() {
  }

  printPolynomial() {
    this.polynomialString = '';
    for (const a in this.polynomial) {
      this.polynomialString = this.polynomialString + this.polynomial[a] + 'x^' + a + ' + ';
    }
    this.polynomialString = this.polynomialString.substr(0, this.polynomialString.length - 3);
  }

  clearInputs() {
    this.polynomialString = '';
    this.polynomial = [];
    this.rangeStart = null;
    this.rangeEnd = null;
    this.accuracy = null;
  }
  calculate() {
    let body;
    if (this.polynomial[0] === 'sin' ) {
      body = JSON.stringify({
        type: this.selectedType, rangeStart: this.rangeStart, rangeEnd: this.rangeEnd, accuracy: this.accuracy,
        polynomial: null});
      console.log('sinus ' + body );
    } else {
      body = JSON.stringify({
        type: this.selectedType, rangeStart: this.rangeStart, rangeEnd: this.rangeEnd, accuracy: this.accuracy,
        polynomial: this.polynomial});
      console.log('normal ' + body );
    }
    // console.log(body);
    if (this.polynomial !== [] && this.rangeEnd != null && this.rangeStart != null && this.accuracy != null && this.selectedType != null) {
      this.http.postBisectionRequest(
        body).subscribe((res: any) => {
          if (res.polynomial.length !== 0) {
            // console.log(res);
            this.request = res.polynomial;
            this.p.request.next(res.polynomial);
            this.p.polynomial.next(this.polynomial);
            this.p.rangeEnd.next(this.rangeEnd);
            this.p.rangeStart.next(this.rangeStart);
          } else {
            this.p.polynomial.next(this.polynomial);
          }
      });
      this.http.postRegulaFalsiRequest(
        body).subscribe((res: any) => {
        if (res.polynomial.length !== 0) {
          // console.log(res);
          this.request = res.polynomial;
          this.r.request.next(res.polynomial);
          this.r.polynomial.next(this.polynomial);
          this.r.rangeEnd.next(this.rangeEnd);
          this.r.rangeStart.next(this.rangeStart);
          this.r.lines.next(res.lines);
        } else {
          alert('Brak wyników w tym zakresie');
          this.r.polynomial.next(this.polynomial);
        }
      });
    } else {
      alert('Podaj prawidłowe dane!');
    }
  }
  // }
}
