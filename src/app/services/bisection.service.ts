import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BisectionService {
  polynomial: BehaviorSubject<Array<number>> = new BehaviorSubject([]);
  rangeStart: BehaviorSubject<number> = new BehaviorSubject(0);
  rangeEnd: BehaviorSubject<number> = new BehaviorSubject(0);
  request: BehaviorSubject<any> = new BehaviorSubject(null);
  it = 0;
  startingData;
  constructor() {
  }
  calculateSin(x) {
    let result = 0;
    this.polynomial.getValue().forEach((a, i) => {
      result = Math.sin(a);
    });
    return result;
  }
  calculateY(x) {
    let result = 0;
    this.polynomial.getValue().forEach((a, i) => {
      result = result + a * Math.pow(x, i);
    });
    return result;
  }

  getRangeEnd() {
    return this.rangeEnd.getValue();
  }
  getIt() {
    return this.it + 1;
  }
  getSteps() {
    return this.request.getValue();
  }
  getRangeStart() {
    return this.rangeStart.getValue();
  }

  getNextIteration() {
    console.log(this.request.getValue());
    let res;
    if (this.it < this.request.getValue().length - 1) {
      res = this.request.getValue()[this.it];
      this.it = this.it + 1;
    } else {
      res = this.request.getValue()[this.it];
    }
    return res;
  }
  getPreviousIteration() {
    console.log(this.request.getValue());
    let res;
    if (this.it > 0) {
      res = this.request.getValue()[this.it];
      this.it = this.it - 1;
    } else if (this.it === 0) {
      return 0;
    } else {
      res = this.request.getValue()[this.it];
    }
    return res;
  }
  getActualRange() {
    return this.request.getValue()[this.it];
  }
  getNextKey() {
    if (this.it !== this.request.getValue().length - 1) {
      return this.request.getValue()[this.it + 1];
    } else {
      return this.request.getValue()[this.it];
    }
  }
  getPreviousKey() {
    if (this.it > 0) {
      return this.request.getValue()[this.it - 1];
    } else {
      return this.request.getValue()[this.it];
    }
  }
  clear() {
    this.polynomial.next([]);
    this.rangeStart.next(1);
    this.rangeEnd.next(2);
    this.request.next(null);
    this.it = 0;
  }
}
