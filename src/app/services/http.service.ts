import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  url = 'http://localhost:8085';
  // url = 'http://idove.pl:8085';
  constructor(private http: HttpClient) {}
  postBisectionRequest(body) {
    return this.http.post(this.url + '/math/bisection', body, {headers: new HttpHeaders().append('Content-type', 'application/json')});
  }
  postRegulaFalsiRequest(body) {
    return this.http.post(this.url + '/math/falsi', body, {headers: new HttpHeaders().append('Content-type', 'application/json')});
  }
}
