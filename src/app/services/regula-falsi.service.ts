import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegulaFalsiService {
  polynomial: BehaviorSubject<Array<number>> = new BehaviorSubject([]);
  rangeStart: BehaviorSubject<number> = new BehaviorSubject(null);
  rangeEnd: BehaviorSubject<number> = new BehaviorSubject(null);
  request: BehaviorSubject<any> = new BehaviorSubject(null);
  it = 0;
  startingData;
  lines: BehaviorSubject<Array<any>> = new BehaviorSubject(null);
  constructor() {
  }

  calculateY(x) {
    let result = 0;
    this.polynomial.getValue().forEach((a, i) => {
      result = result + a * Math.pow(x, i);
    });
    return result;
  }
  getIt() {
    return this.it + 1;
  }
  getRangeEnd() {
    return this.rangeEnd.getValue();
  }
  getRangeStart() {
    return this.rangeStart.getValue();
  }
  getNextIteration() {
    console.log(this.request.getValue());
    let res;
    if (this.it < this.request.getValue().length - 1) {
      res = {polynomial : this.request.getValue()[this.it], lines: this.lines.getValue()[this.it]};
      this.it = this.it + 1;
    } else {
      res = {polynomial : this.request.getValue()[this.it], lines: this.lines.getValue()[this.it]};
    }
    return res;
  }
  getPreviousIteration() {
    console.log(this.request.getValue());
    let res;
    if (this.it > 0) {
      res = {polynomial : this.request.getValue()[this.it], lines: this.lines.getValue()[this.it]};
      this.it = this.it - 1;
    } else if (this.it === 0) {
      return 0;
    } else {
      res = {polynomial : this.request.getValue()[this.it], lines: this.lines.getValue()[this.it]};
    }
    return res;
  }
  getSteps() {
    return this.request.getValue();
  }
  clear() {
    this.polynomial.next([]);
    this.rangeStart.next(1);
    this.rangeEnd.next(2);
    this.request.next(null);
    this.it = 0;
    this.lines.next(null);
  }
}
