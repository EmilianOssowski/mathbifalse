import  { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BisectionComponent } from './components/bisection/bisection.component';
import {ChartModule} from 'primeng/chart';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormComponent } from './components/form/form.component';
import {ButtonModule, ChipsModule, DropdownModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { RegulaFalseComponent } from './components/regula-false/regula-false.component';
import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import { PlotlyModule } from 'angular-plotly.js';
import {BisectionService} from "./services/bisection.service";
import {HttpService} from "./services/http.service";
import {RegulaFalsiService} from "./services/regula-falsi.service";
import {TableModule} from 'primeng/table';

PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
  declarations: [
    AppComponent,
    BisectionComponent,
    FormComponent,
    RegulaFalseComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ChartModule,
    DropdownModule,
    InputTextModule,
    KeyFilterModule,
    ButtonModule,
    ChipsModule,
    PlotlyModule,
    TableModule
  ],
  providers: [BisectionService, HttpService, RegulaFalsiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
